This is the MS3 Contact System API. 

Problem Statement:
MS3 is in need of a contact management API.  This API will take the following data model and will commit it to a database.  We need this to support the full CRUD operations but also full and incremental updates.  The DBA is out of town for the next week and we need this ASAP.  Take liberty to design the database components that will support the API and data model.  We need this to follow API Best practices as this will be customer facing and high visibility.

One of our corporate goals is to not build pointed solutions.  Keep in mind that we want to be able to reuse components to support other development and API initiatives in the organization.

{
	"Identification": {
		"FirstName": "Bob",
		"LastName": "Frederick",
		"DOB": "06/21/1980",
		"Gender": "M",
		"Title": "Manager"
	},
	"Address": [{
		"type ": "home",
		"number": 1234,
		"street": "blah blah St",
		"Unit": "1 a",
		"City": "Somewhere",
		"State": "WV",
		"zipcode": "12345"
	}],
	"Communication": [{
			"type": "email",
			"value": "bfe@sample.com",
	  		"preferred" : "true"
		},
		{
			"type": "cell",
			"value": "304-555-8282"
		}
	]
}

Minimum Requirements
Code should be submitted via email with a link to a Github or Bitbucket repository
Please utilize MuleSoft to accomplish this task
Provide instructions for running the application(s) should be listed in a README.md
API Must be a RESTful API that adheres to industry best practices
Provide SQL statement for table design
Provide diagram for table association

“Nice to Have” Requirements:
UI/Web Application
Get creative with this portion of the project, how would you best utilize/demonstrate this API
What we’ve seen in the past
HTML should have form based input fields
HTML should have a submit button
HTML should have a table that represents the (R)ead response of data
HTML should show the response information of CUD operations

Bonus Requirements
Provide Contact sorting in UI
Working Cloud deployment
AWS, Azure or Google Cloud
Dockerized Containers
CI/CD script
Unit Test cases


Solution:

To Run this application on your machine, Please pass below properties in runtime.
-M-XX:-UseBiasedLocking -Dmule.env=dev -Dkey=MS3muleDev@2k19! -Dhttp.port=8081 -Dhttp.private.port=8091

scripts:

Identification: SQLQueryDCIdentification.sql
USE [MS3Contacts]
GO

ALTER TABLE [dbo].[Identification] DROP CONSTRAINT [CK__Identific__Gende__10566F31]
GO

/****** Object:  Table [dbo].[Identification]    Script Date: 10/20/2019 1:39:59 PM ******/
DROP TABLE [dbo].[Identification]
GO

/****** Object:  Table [dbo].[Identification]    Script Date: 10/20/2019 1:39:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Identification](
	[ContactId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nchar](20) NOT NULL,
	[LastName] [nchar](20) NOT NULL,
	[DOB] [date] NOT NULL,
	[Gender] [varchar](1) NOT NULL,
	[Title] [nchar](20) NOT NULL,
 CONSTRAINT [PK_Identification] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Identification]  WITH CHECK ADD CHECK  (([Gender]='F' OR [Gender]='M'))
GO

Address: SQLQueryDCAddress.sql
USE [MS3Contacts]
GO

ALTER TABLE [dbo].[Address] DROP CONSTRAINT [FK_Address_Identification]
GO

/****** Object:  Table [dbo].[Address]    Script Date: 10/20/2019 1:34:04 PM ******/
DROP TABLE [dbo].[Address]
GO

/****** Object:  Table [dbo].[Address]    Script Date: 10/20/2019 1:34:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Address](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,
	[ContactId] [int] NOT NULL,
	[Type] [nchar](10) NOT NULL,
	[Number] [nchar](10) NOT NULL,
	[Street] [nvarchar](50) NOT NULL,
	[Unit] [nchar](10) NULL,
	[City] [nvarchar](50) NOT NULL,
	[State] [nchar](2) NOT NULL,
	[ZipCode] [nchar](5) NOT NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Identification] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Identification] ([ContactId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_Identification]
GO

CommunicationType: SQLQueryDCCommunicationType.sql
USE [MS3Contacts]
GO

ALTER TABLE [dbo].[CommunicationType] DROP CONSTRAINT [CK__Communicat__Type__236943A5]
GO

ALTER TABLE [dbo].[CommunicationType] DROP CONSTRAINT [CK__Communica__Prefe__22751F6C]
GO

ALTER TABLE [dbo].[CommunicationType] DROP CONSTRAINT [FK_CommunicationType_Identification]
GO

ALTER TABLE [dbo].[CommunicationType] DROP CONSTRAINT [DF_CommunicationType_Preferred]
GO

/****** Object:  Table [dbo].[CommunicationType]    Script Date: 10/20/2019 1:38:10 PM ******/
DROP TABLE [dbo].[CommunicationType]
GO

/****** Object:  Table [dbo].[CommunicationType]    Script Date: 10/20/2019 1:38:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CommunicationType](
	[ComTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nchar](10) NOT NULL,
	[Value] [nchar](50) NOT NULL,
	[Preferred] [nchar](5) NULL,
	[ContactId] [int] NOT NULL,
 CONSTRAINT [PK_CommunicationType] PRIMARY KEY CLUSTERED 
(
	[ComTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CommunicationType] ADD  CONSTRAINT [DF_CommunicationType_Preferred]  DEFAULT ((1)) FOR [Preferred]
GO

ALTER TABLE [dbo].[CommunicationType]  WITH CHECK ADD  CONSTRAINT [FK_CommunicationType_Identification] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Identification] ([ContactId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CommunicationType] CHECK CONSTRAINT [FK_CommunicationType_Identification]
GO

ALTER TABLE [dbo].[CommunicationType]  WITH CHECK ADD  CONSTRAINT [CK__Communica__Prefe__22751F6C] CHECK  (([Preferred]='false' OR [Preferred]='true'))
GO

ALTER TABLE [dbo].[CommunicationType] CHECK CONSTRAINT [CK__Communica__Prefe__22751F6C]
GO

ALTER TABLE [dbo].[CommunicationType]  WITH CHECK ADD  CONSTRAINT [CK__Communicat__Type__236943A5] CHECK  (([Type]='email' OR [Type]='cell'))
GO

ALTER TABLE [dbo].[CommunicationType] CHECK CONSTRAINT [CK__Communicat__Type__236943A5]
GO


Secure Configuration Properties
https://docs.mulesoft.com/mule-runtime/4.1/secure-configuration-properties
**key = This is the value used to encrypt the properties in yaml files.
key=MS3muleDev@2k19!

Encrypt Commmand: java -jar secure-properties-tool.jar string encrypt AES CBC MS3muleDev@2k19! <your-password>

 **Open ms3-contact-mssql-sys-api-dev.yaml file** located in src/main/resources/env directory. Open Database Configuration in Global Elements tab and configure the connection attributes:

		host: ms3.czephssyxpsl.us-east-1.rds.amazonaws.com
		port: 1433
		user: muleIntegration
		password: ![ufInTzkkB5oFn6EHJCzbVA==]

<db:config name="Database_Config" doc:name="Database Config" doc:id="44aad9fc-a0a1-49c6-a14f-b69fef32b34d" >
		<db:mssql-connection host="${secure::database.host}" port="${secure::database.port}" user="${secure::database.user}" password="${secure::database.password}" databaseName="${secure::database.name}"/>
</db:config>